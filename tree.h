#ifndef BINARYTREE_TREE_H
#define BINARYTREE_TREE_H

#include <string>

class Node {
    friend class Tree;

    int key;
    std::string value;
    Node * leftChild;
    Node * rightChild;

    Node(int key, std::string value);
};

class Tree {
    Node * root;

    void printResult(Node * node);
    void inOrderTraverse(Node * currentNode);
    void postOrderTraverse(Node * currentNode);
    std::string * search(int key, Node * currentNode);
    void insert(int key, std::string value, Node * currentNode);
    void remove(int key, Node * currentNode, Node * parentNode);

public:
    Tree();

    void inOrderTraverse();
    void postOrderTraverse();
    std::string * search(int key);
    void insert(int key, std::string value);
    void remove(int key);
};

#endif //BINARYTREE_TREE_H