#include "tree.h"

Node::Node(int key, std::string value) : key(key), value(std::move(value)), leftChild(nullptr), rightChild(nullptr) {}

Tree::Tree() {
    Node * node17 = new Node(17, "p17");
    Node * node16 = new Node(16, "p16");
    node16->rightChild = node17;

    Node * node18 = new Node(18, "p18");
    node18->leftChild = node16;

    Node * node23 = new Node(23, "p23");
    node18->rightChild = node23;

    Node * node15 = new Node(15, "p15");
    node15->rightChild = node18;

    root = node15;

    Node * node7 = new Node(7, "p7");
    Node * node12 = new Node(12, "p12");
    node12->leftChild = node7;

    Node * node14 = new Node(14, "p14");
    node12->rightChild = node14;

    node15->leftChild = node12;
}

void Tree::printResult(Node *node) {
    printf("key: %d, value: %s\n", node->key, node->value.c_str());
}

void Tree::inOrderTraverse(Node *currentNode) {
    if (currentNode != nullptr) {
        inOrderTraverse(currentNode->leftChild);
        printResult(currentNode);
        inOrderTraverse(currentNode->rightChild);
    }
}

void Tree::postOrderTraverse(Node *currentNode) {
    if (currentNode != nullptr) {
        postOrderTraverse(currentNode->rightChild);
        printResult(currentNode);
        postOrderTraverse(currentNode->leftChild);
    }
}

void Tree::inOrderTraverse() {
    inOrderTraverse(root);
}

void Tree::postOrderTraverse() {
    postOrderTraverse(root);
}

std::string *Tree::search(int key, Node *currentNode) {
    if (currentNode != nullptr) {
        if (currentNode->key == key) {
            return &currentNode->value;
        } else if (key < currentNode->key) {
            return search(key, currentNode->leftChild);
        } else if (key > currentNode->key) {
            return search(key, currentNode->rightChild);
        }
    }

    return nullptr;
}

std::string *Tree::search(int key) {
    return search(key, root);
}

void Tree::insert(int key, std::string value, Node *currentNode) {

    if (key > currentNode->key) {
        if (currentNode->rightChild == nullptr) {
            Node *node = new Node(key, value);
            currentNode->rightChild = node;
        } else {
            insert(key, value, currentNode->rightChild);
        }

    } else if (key < currentNode->key) {
        if (currentNode->leftChild == nullptr) {
            Node *node = new Node(key, value);
            currentNode->leftChild = node;
        } else {
            insert(key, value, currentNode->leftChild);
        }

    } else {
        Node *node = new Node(key, value);
        node->leftChild = currentNode->leftChild;
        node->rightChild = currentNode->rightChild;

        currentNode->leftChild = nullptr;
        currentNode->rightChild = nullptr;

        currentNode = node;
    }
}

void Tree::insert(int key, std::string value) {
    insert(key, value, root);
}

void Tree::remove(int key, Node *currentNode, Node *parentNode) {
    if (currentNode != nullptr) {
        if (key < currentNode->key) {
            remove(key, currentNode->leftChild, currentNode);
        } else if (key > currentNode->key) {
            remove(key, currentNode->rightChild, currentNode);
        } else {
            if (currentNode == parentNode->leftChild) {

                if (currentNode->leftChild == nullptr && currentNode->rightChild != nullptr) {
                    parentNode->rightChild = currentNode->rightChild;
                } else if (currentNode->rightChild == nullptr && currentNode->leftChild != nullptr) {
                    parentNode->leftChild = currentNode->leftChild;
                } else {

//                        find successor and move subtree

                }

            } else if (currentNode == parentNode->rightChild) {
                if (currentNode->rightChild == nullptr && currentNode->leftChild != nullptr) {
                    parentNode->leftChild = currentNode->leftChild;
                } else if (currentNode->leftChild == nullptr && currentNode->rightChild != nullptr) {
                    parentNode->rightChild = currentNode->rightChild;
                } else {

//                        find successor and move subtree

                }
            }
        }


    }
}

void Tree::remove(int key) {
    remove(key, root, nullptr);
}
